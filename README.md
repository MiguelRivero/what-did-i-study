# What did I study?

##Math:
Linear Algebra

Numerical Calculus

Introduction to Discrete Mathematics

Discrete Mathematics

Introduction to Infinitesimal Calculus

Infinitesimal Calculus

Linear Systems

## Introduction to Programming I - Introduction to Programming II
* Introduction to OOP
* Elements of the Java language
* Types design
* Collections
* Sequential treatments
* Implementation and reuse
* Factories
* Iterables
* Expressions and sort orders
* Generalized sequential treatments
* Composition iterables
* Introduction to the programming language C

## Algorithom Analysis and  Design - Análisis y Diseño de Algoritmos -
* Block 1: Recursion
* Block 2: Analysis of algorithms
* Block 3: Considerations on the technical design of algorithms
* Block 4: Divide and conquer
* Block 5: greedy algorithms
* Block 6: Backtracking Algorithms
* Block 7: Dynamic Programming

## Data Structures and Algorithms - EDA
* Block 1: Data Structures
* Block 2: Trees and Functions
* Block 3: Tours in graphs

## Statistics
* Block 1: Descriptive statistics
    1. Topic 1: Statistics. Definitions and Concepts
    1. Topic 2: numerical and graphical summaries
    1. Topic 3: Measures Statistics
    1. Topic 4: Outliers. Basic methods for treatment
    1. Topic 5: Bivariate Distributions
    1. Topic 6: Correlation and Regression Analysis
    1. Topic 7: Index Numbers
* Block 2: Calculation of probabilities
    1. Topic 8: randomized experiments. probability
    1. Topic 9: Random Variable. Distribution Function. associated features
    1. Topic 10: Models distributions
* Block 3: Statistical Inference
    1. Topic 11: Introduction to Statistical Inference
    1. Topic 12: Distributions associated with sampling in normal populations
    1. Topic 13: Point estimation
    1. Topic 14: Estimation by confidence regions
    1. Topic 15: Tests of statistical hypotheses

## Business Software Engineering I
* Block 1: Introduction To Software Engineering.
* Block 2: Introduction To Engineering Requirements. Requirements Elicitation Systems Software.
* Block 3: System Requirements Analysis Software


## Communications I
* Block 1: Block I: Introduction to communications.
    1. Topic 1: Basic Concepts of Information and Communications.
    1. Topic 2: OSI Model.
    1. Topic 3: Local Area Networks.
* Block 2: Block II: Layer 1 of the OSI model.
    1. Topic 4: Layer 1 - Electronics and Signals.
    1. Topic 5: Layer 1 - Media, Connections and collisions.
* Block 3: Block III: Layer 2 of the OSI model.
    1. Topic 6: Layer 2 - Concepts
    1. Topic 7: Layer 2 - Technologies

## Communications II
* Block 1: Block I: Introduction.
    1. Topic 1:  Introduction and preconceptions.
* Block 2: Block II: The network layer.
    1. Topic 2: Layer 3 - Routing and Addressing.
    1. Topic 3: Layer 3 - Protocols.
* Block 3: Block III: Upper Levels.
    1. Topic 4: Layer 4 - The Transport Layer.
    1. Topic 5: Layers 5, 6 and 7 Session, Presentation, Application.
* Block 4: Block IV: Configuring routers.
    1. Topic 6: Configuring routers.



## Business Software Engineering II
* Block 1: Introduction to Design
* Block 2: Refactoring
* Block 3: Testing and maintenance
* Block 4: Detailed Design
* Block 5: Architectural Design
* Block 6: Practices
* Block 7: Software Development Team

## Business Software Engineering III
* Block 1: Implementation of a Distributed Information System (DIS)
    1. Topic 1: Architectural Patterns.
    1. Topic 2: Design logical layer.
    1. Topic 3: Design the presentation layer.
    1. Topic 4: Design the data layer.
* Block 2: Planning and Estimation.
    1. Topic 5: Planning and Estimation

## Database
* Module I. Fundamentals.
    1. Topic 1: Introduction to databases.
    1. Topic 2: Architecture of a BD Manager: Architecture ANSI / X3 / SPARC centralized.
    1. Topic 3: Data Models.
    1. Topic 4: Introduction to model ENTITY / RELATIONSHIP Chen.
* Module II .: Relational Model.
    1. Topic 5: Structures and constraints in the relational model.
    1. Topic 6: Relational Algebra.
    1. Topic 7: Relational Calculus: Tuple Calculus, Calculus Domain, SQL and QBE vs Relational Calculus.
    1. Topic 8: SQL / ISO: definition and manipulation of SQL DB.
    1. Topic 9: Introduction to Standardization BDR: Normal forms based on functional dependencies (1NF, 2NF, 3NF, BCNF)

## Database Design
* Module I: DESIGN BD
    1. Topic 1: The process of creating a BD
    1. Topic 2: Analysis of a methodology for the design of BD
    1. Topic 3: Conceptual Modeling: Model E / R extended (SE / R), Case conceptual design with EE / R model
    1. Topic 4: Technological Design BD: Transforming conceptual schema standard logical schema (SQL / ISO) and owner ORACLE SERVER.
* Module II: INTRODUCTION TO ENVIRONMENTAL MANAGEMENT SYSTEMS DATABASE
    1. Topic 5: Security
    1. Topic 6: Concurrency
    1. Topic 7: Recovery
    1. Topic 8: Consultation process in Relational DBMS

## Web Technologies - Ampliacion de Databases
* Block 1: Basic Concepts of Web Applications
    1. Topic 1: Introduction to Web Applications
* Block 2: Presentation Layer
    1. Topic 2: Introduction to (X) HTML
    1. Topic 3: Forms in (X) HTML
    1. Topic 4: Introduction to CSS Style Sheets
    1. Topic 5: Introduction to (X) HTML Dynamic: DOM & JavaScript
* Block 3: Business Logic Layer
    1. Topic 6: Processing Web Servers
    1. Topic 7: Accessing Databases from Web Servers
* Block 4: Integration of Practice Course
    1. Topic 8: Integration Practice Course

## Graph Theory
* Block 1: Fundamentals.
    1. Topic 1: Graphs and algorithms.
    1. Topic 2: Trees
    1. Topic 3: Roads and distances
    1. Topic 4: Layout element distribution
* Block 2: Satisfiability and orthogonal connections
    1. Topic 5: Satisfiability
    1. Topic 6: Connections Orthogonal
* Block 3: Spanners: spanning graphs
    1. Topic 7: Spanners
* Block 4: Small World
    1. Topic 8: The small world experiment
    1. Topic 9: Grade-diameter

## Operating Systems
* Block 1: Introduction and Process
    1. Topic 1: Introduction
    1. Topic 2: Fundamentals.
    1. Topic 3: Processes.
    1. Topic 4: Planning Process
    1. Topic 5: Other aspects of planning
* Block 2: Concurrency and deadlock
    1. Topic 6: Concurrence Process.
    1. Topic 7: Concurrency and synchronization.
    1. Topic 8: Synchronization and communication.
    1. Topic 9: Deadlock.
* Block 3: Management and Memory Management
    1. Topic 10: Memory Management.
    1. Topic 11: Segmentation and paging memory
    1. Topic 12: Virtual Memory
* Block 4: Input / Output and File Systems
    1. Topic 13: Input / Output
    1. Topic 14: Management of reading / writing.
    1. Topic 15: File Management
    1. Topic 16: File Servers and Other Concepts
    1. Topic 17: Security and Protection System Files
* Block 5: Practice
    1. Lab 1: Introduction to UNIX
    1. Practice 2: Compiler gcc.
    1. Practice 3: Files.
    1. Practice 4: Processes, signals and pipes.
    1. Practice 5: Traffic.
    1. Practice 6: Messages between processes

## Artificial Inteligence
* Topic 1: Introduction to the Python programming language
* Topic 2: Search
* Topic 3: Planning
* Topic 4: Representation and reasoning with probabilistic knowledge
* Topic 5: Machine Learning
* Topic 6: Neural Networks

## Software Process and Management - PSG
* Topic 1: The software process: introduction; information gathering techniques; process modeling techniques.
* Topic 2: Process models: prescriptive process model; agile process model; Capability Maturity Model Integration; etc.
* Topic 3: Measurement in software process: introduction to measurement; scope of the metrics; Measuring internal product attributes; external measuring product attributes; measuring attributes of resources; process and project metrics; empirical studies.
* Topic 4: Quality Management Software: Introduction; regulatory framework; quality models; procedures for quality control; Checklists, scripts, recommendations and auxiliaries; dossier of quality assurance; general plan of quality assurance.
* Topic 5: Configuration Management: Introduction; activities and tasks; tools, techniques and methods.
* Topic 6: Procurement Management: procurement requirements; planning of the procurement process; preparation of the application; supplier selection; negotiation process; agreements with suppliers; procurement; contract management, contract closure, standards.
* Topic 7: Risk Management: Concepts, risk classification, risk assessment, risk management process, standards.

## Planning and Managing Software Projects - PGPI
* Topic 1: General concepts
* Topic 2: Processes and areas of management and project management
* Topic 3: Management of project startup and shutdown
* Topic 4: Management of the project plan
* Topic 5: Management of project implementation
* Topic 6: Standards, organizations and methodologies. certifications
* Topic 7: Techniques, methods and documentation
* Topic 8: Tools for project management